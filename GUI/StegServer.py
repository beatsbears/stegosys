#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software


import os
import sys
import re
from flask import Flask, request, redirect, render_template, abort, send_from_directory, current_app, jsonify
from werkzeug import secure_filename
sys.path.append( os.path.dirname( os.path.dirname( os.path.abspath(__file__) ) ) )
from StegLibs import StegCommon, StegIMG, StegAIF, StegWav

app = Flask(__name__)

WavH = StegWav.StegWAVHelper()
AifH = StegAIF.StegAIFHelper()
ImgH = StegIMG.StegIMGHelper()

##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________
## CONFIG VALUES

# This is the path to the upload and created directories
app.config['UPLOAD_FOLDER'] = os.path.dirname(os.path.realpath(__file__)) + '/uploads/'
app.config['CREATED_FOLDER'] = returnDirectory = os.path.dirname(os.path.realpath(__file__)) + '/created/'

# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS_HOST'] = WavH.get_supported_file_types() + AifH.get_supported_file_types() + ImgH.get_supported_file_types()
app.config['ALLOWED_EXTENSIONS_TGT'] = ['TXT', 'PDF', 'PNG', 'JPG', 'JPEG', 'GIF', 'ICO', 'WAV', 'AIF', 'AIFF', 'BMP', 'TIF', 'TIFF', 'OCTET-STREAM', 'PY', 'SH']


# server port
app.config['SERVER_PORT'] = '5050'

##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________


## Returns index page
@app.route('/')
def index():
    return render_template('index.html')

##________________________________________________________________________
# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    ## Uploaded form values
    print request.form
    mode = request.form["H_E"]
    encryption_mode = request.form["isEncrypt"]
    encryption_key = request.form["EncryptionKey"]

    ## Dict of uploaded files
    uploaded_files = {}
    if request.files != None:
        uploaded_files["host"] = request.files["Hfile"]
    if mode == "Hide":
        uploaded_files["tgt"] = request.files["Tfile"]

    # Check if the files are of the allowed types/extensions
    if _check_and_save_files(uploaded_files):
        pass
    else:
        abort(500)

    hostFile = app.config['UPLOAD_FOLDER'] + request.files["Hfile"].filename.replace(" ","_")
    hostFile = hostFile.replace(")","")
    hostFile = hostFile.replace("(","")
    tgtFile = app.config['UPLOAD_FOLDER'] + request.files["Tfile"].filename.replace(" ","_")
    tgtFile = tgtFile.replace("(","")
    tgtFile = tgtFile.replace(")","")


    print tgtFile
    print hostFile
    ## Check if hide or extract mode
    if mode == "Hide":
        try:
            result_file = _hide_file(hostFile,tgtFile,encryption_key,encryption_mode,app.config['CREATED_FOLDER'])
        except:
            abort(500)
        finally:
            ## Remove all temporary files
            _cleanup_temp_directory(app.config['UPLOAD_FOLDER'])
        return render_template('file_hidden.html',downloadfilename=result_file)
    elif mode == "Extract":
        try:
            result_file =  _extract_file(hostFile,encryption_key,encryption_mode,app.config['CREATED_FOLDER'])
        except:
            abort(500)
        finally:
            ## Remove all temporary files
            _cleanup_temp_directory(app.config['UPLOAD_FOLDER'])
        return render_template('file_hidden.html',downloadfilename=result_file)
    else:
        abort(500)

##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________
## DOWNLOADS


@app.route('/created/<path:filename>', methods=['GET'])
def download(filename):
    uploads = os.path.join(current_app.root_path, app.config['CREATED_FOLDER'])
    return send_from_directory(directory=uploads, filename=filename)

##________________________________________________________________________
@app.route('/filedownload', methods=['GET'])
def filedownload():
    _cleanup_temp_directory(app.config['CREATED_FOLDER'])
    return redirect("/", code=302)


##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________
## ERRORS

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

##________________________________________________________________________
@app.errorhandler(500)
def error_message(error):
    return render_template('500.html'), 500

##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________
## AJAX File uploads

@app.route('/host_file_ajax', methods=['POST'])
def host_file_ajax():
    host_file_sent = {}
    if request.files != None:
        host_file_sent['host'] = request.files["Hfile"]
        if _check_and_save_files(host_file_sent):
            return jsonify(filename=host_file_sent['host'].filename,
                           support="Yes",
                           size=0,
                           capacity=0)
        else:
            return jsonify(filename=host_file_sent['host'].filename,
                           support="No",
                           size=0,
                           capacity=0
                           )

##________________________________________________________________________
@app.route('/tgt_file_ajax', methods=['POST'])
def tgt_file_ajax():
    tgt_file_sent = {}
    if request.files != None:
        tgt_file_sent['tgt'] = request.files["Tfile"]
        if _check_and_save_files(tgt_file_sent):
            return jsonify(filename=tgt_file_sent['tgt'].filename,
                           support="Yes",
                           size=0)
        else:
            return jsonify(filename=tgt_file_sent['tgt'].filename,
                           support="No",
                           size=0)

##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________

def _check_and_save_files(filedict):
    print filedict
    for t,f in filedict.items():
        print f.filename
        if f.filename == '':
            return False
        if ((t == 'host') and allowed_file(f.filename,app.config['ALLOWED_EXTENSIONS_HOST'])) or ((t == 'tgt') and allowed_file(f.filename,app.config['ALLOWED_EXTENSIONS_TGT'])):
            try:
                # Make the filename safe, remove unsupported chars
                filename = secure_filename(f.filename)
                # Move the file to the uploads file
                f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            except Exception, e:
                print str(e)
                return False
        else:
            print f.filename, t
            return False
    # return true if we get them all
    print "save successful"
    return True

##________________________________________________________________________
# returns allowed files
def allowed_file(filename,allowedExtensions):
    print filename, allowedExtensions
    return '.' in filename and filename.rsplit('.', 1)[1].upper() in allowedExtensions



##________________________________________________________________________
def _hide_file(host,tgt,key,mode,location):
    ## create and seed StegCommon
    stegCom = StegCommon.StegCommon(host,key,mode)
    stegCom._seed_supported_types(WavH.get_supported_file_types_dict(),AifH.get_supported_file_types_dict(),ImgH.get_supported_file_types_dict())
    hideObj = stegCom.return_correct_object(tgt)
    if hideObj.__class__.__name__ == "StegIMG":
        hideObj.open_image(host)
        hideObj._use_correct_function_hide(hideObj.fg,tgt,stegCom.get_file_extension_host(),location)
        return 'newImg.' + stegCom.get_file_extension_host()
    elif hideObj.__class__.__name__ == "StegAIF" or hideObj.__class__.__name__ == "StegWav":
        hideObj._open_audio(host)
        hideObj._replace_bits(tgt,stegCom.get_file_extension_host(),location)
        return 'new_audio.' + stegCom.get_file_extension_host()



##________________________________________________________________________
def _extract_file(host,key,mode,location):
    ## create and seed StegCommon
    stegCom = StegCommon.StegCommon(host,key,mode)
    stegCom._seed_supported_types(WavH.get_supported_file_types_dict(),AifH.get_supported_file_types_dict(),ImgH.get_supported_file_types_dict())
    hideObj = stegCom.return_correct_object()
    if hideObj.__class__.__name__ == "StegIMG":
        hideObj.open_image(host)
        return_file = hideObj._use_correct_function_extract(hideObj.fg, location)
        if return_file != '.':
            return return_file
    elif hideObj.__class__.__name__ == "StegAIF" or hideObj.__class__.__name__ == "StegWav":
        hideObj._open_audio(host)
        return_file = hideObj._extract_message()
        if return_file != '.':
            return return_file

##________________________________________________________________________
def _cleanup_temp_directory(directory):
    uploadedToBeDeleted = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]
    for file in uploadedToBeDeleted:
        if file[0] == '.':
            pass
        else:
            print 'deleting temp file: {}'.format(file)
            os.remove(directory + file)

##________________________________________________________________________
def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')



##________________________________________________________________________
##________________________________________________________________________
##________________________________________________________________________

if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int(app.config['SERVER_PORT']),
        debug=True
    )