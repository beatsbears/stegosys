try:
    import cefpython3.wx.chromectrl as chrome
except: 
    exit(0)

import os
import wx
import platform


APP_NAME = 'Stegosys'
APP_VERSION = '0.1 ALPHA'
SEVRER_URL = 'http://localhost:5050'

class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent=None, id=wx.ID_ANY, title=APP_NAME, size=(800,700))

        self.cefWindow = chrome.ChromeWindow(self, url=(SEVRER_URL))
        sizer = wx.BoxSizer()
        sizer.Add(self.cefWindow, 1, wx.EXPAND, 0)
        self.SetSizer(sizer)
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        # Create menu bar --------------------------------------------------------------------------------
        menubar = wx.MenuBar()
        menubar.SetName(APP_NAME)
        helpMenu = wx.Menu()
        helpMenu.Append(wx.ID_HELP,'&Help\tCtrl+H')
        helpMenu.Append(wx.ID_ABOUT, "&About\tShift+Ctrl+A")
        menubar.Append(helpMenu, 'Help')
        wx.EVT_MENU(self, wx.ID_HELP, self.openHelp)
        wx.EVT_MENU(self, wx.ID_ABOUT, self.openAbout)
        wx.EVT_MENU(self, wx.ID_EXIT, self.OnQuit)
        self.SetMenuBar(menubar)


    def OnClose(self, event):
        # Remember to destroy all CEF browser references before calling Destroy()
        self.Destroy()
        self.Close()
        # On Mac the code after app.MainLoop() never executes, so need to call CEF shutdown here.
        if platform.system() == "Darwin":
            chrome.Shutdown()
            wx.GetApp().Exit()

    ## Quit application
    def OnQuit(self, e):
        self.OnClose(e)

       ## Help Dialog
    def openHelp(self,e):
        print "open Help"
        pass

    ## About Dialog
    def openAbout(self,e):

        #supportedFileFormats = StegIMG.StegIMGHelper().get_supported_file_types()
        description = """{} is a GUI application based on the open source pysteg project which aims
        to enable users to hide messages and files within a variety of different mediums.
        """.format(APP_NAME)

        licence = """{} is free software; you can redistribute
        it and/or modify it under the terms of the GNU General Public License as
        published by the Free Software Foundation; either version 2 of the License,
        or (at your option) any later version.

        {} is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU General Public License for more details. You should have
        received a copy of the GNU General Public License along with {};
        if not, write to the Free Software Foundation, Inc., 59 Temple Place,
        Suite 330, Boston, MA  02111-1307  USA



        See source code at: https://bitbucket.org/beatsbears/stegosys""".format(APP_NAME,APP_NAME,APP_NAME)


        info = wx.AboutDialogInfo()

        #info.SetIcon(wx.Icon('hunter.png', wx.BITMAP_TYPE_PNG))
        info.SetName(APP_NAME)
        info.SetVersion(APP_VERSION)
        info.SetDescription(description)
        info.SetCopyright('(C) 2016 Andrew Scott')
        info.SetWebSite('http://drownedcoast.xyz')
        info.SetLicence(licence)
        info.AddDeveloper('Andrew Scott')

        wx.AboutBox(info)

class BasicApp(wx.App):
    def OnInit(self):
        frame = MainFrame()
        self.SetTopWindow(frame)
        frame.Show()
        return True

if __name__ == '__main__':
    chrome.Initialize({
        "debug": True,
        "log_file": "debug.log",
        "log_severity": chrome.cefpython.LOGSEVERITY_INFO,
        "release_dcheck_enabled": True,
    })

    # Start local browser
    app = BasicApp(False)

    app.MainLoop()



    # Important: do the wx cleanup before calling Shutdown
    del app

    # On Mac Shutdown is called in OnClose
    if platform.system() in ["Linux", "Windows"]:
        chrome.Shutdown()
