

var operation_mode = "Hide";


function LoadingGif(){
    var GifDiv = document.getElementById("loading");
    var MainApp = document.getElementById("mainApp");

    var show = function(){
    MainApp.style.display = "none";
    GifDiv.style.display = "block";
    setTimeout(hide, 2000);  // 2 seconds
    }

    var hide = function(){
    $("#mainApp").fadeIn()
    $("#loading").fadeOut()
    }

    show();
};


function displayHostFile() {
    var host_label = document.getElementById("host_file_label");
    var host_file = document.getElementById("host_file_input");
    var host_details_field = document.getElementById("host_details");

    host_file.onchange = function(){
         var bad_string = host_file.value;
         var res = bad_string.split("\\");
         host_label.value = "  " + res[res.length - 1];

        event.preventDefault();

        var files = host_file.files;

        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('Hfile', file, file.name);
            }

        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'host_file_ajax', true);

        xhr.onload = function () {
            if (xhr.status == 200) {
            console.log(xhr.status);
            console.log(xhr.response);
            var json_response = JSON.parse(xhr.response);
            var filename = json_response.filename;
            var support = json_response.support;
            var size = json_response.size;
            var capacity = json_response.capacity;

            console.log(filename);
            host_details.value = ("File Name: " + filename);
            host_details.value += ("\nFile Type Supported: " + support);
            host_details.value += ("\nFile Size: " + size);
            host_details.value += ("\nHiding Capacity: " + capacity);
            } else {
                console.error("Bad response!");
            }
        };
        xhr.send(formData);

    }
};

function displayTargetFile() {
    var tgt_label = document.getElementById("tgt_file_label");
    var tgt_file = document.getElementById("tgt_file_input");
    var tgt_details_field = document.getElementById("tgt_details");

    tgt_file.onchange = function(){
         var bad_string = tgt_file.value;
         var res = bad_string.split("\\");
         tgt_label.value = "  " + res[res.length - 1];

              event.preventDefault();

        var files = tgt_file.files;

        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('Tfile', file, file.name);
            }

        var xhr = new XMLHttpRequest();

        xhr.open('POST', 'tgt_file_ajax', true);

        xhr.onload = function () {
            if (xhr.status == 200) {
            console.log(xhr.status);
            console.log(xhr.response);
            var json_response = JSON.parse(xhr.response);
            var filename = json_response.filename;
            var support = json_response.support;
            var size = json_response.size;


            console.log(filename);
            tgt_details.value = ("File Name: " + filename);
            tgt_details.value += ("\nFile Type Supported: " + support);
            tgt_details.value += ("\nFile Size: " + size);
            } else {
                console.error("Bad response!");
            }
        };
        xhr.send(formData);

    }
};

function hideTargetInputField(){
    var selected_mode = document.getElementById("myonoffswitch");
    var display_checkbox = document.getElementById("checkbox_file_details");
    var tgt_label = document.getElementById("tgt_file_label");

    selected_mode.onchange = function(){
        operation_mode = getMode();
        // extra logging
        if (!selected_mode.checked) {
            $("#tgt_file").fadeIn();
            if ((display_checkbox.checked) && (tgt_label.value != "")) {
                $("#tgt_details").slideDown("slow");
            }
        } else {
            $("#tgt_file").fadeOut();
            $("#tgt_details").slideUp();
        }
    }

};

function hideEncryptionPass() {
    var selected_encryption_mode = document.getElementById("enc_mode");

    selected_encryption_mode.onchange = function(){
        if (selected_encryption_mode.value == "None") {
            $("#encryption_pass_label").fadeOut();
            $("#encryption_pass_input").fadeOut();
        } else {
            $("#encryption_pass_label").fadeIn();
            $("#encryption_pass_input").fadeIn();
             }
    }
};

function displayFileDetails() {
    var display_checkbox = document.getElementById("checkbox_file_details");
    var host_label = document.getElementById("host_file_label");
    var tgt_label = document.getElementById("tgt_file_label");
    var selected_mode = document.getElementById("myonoffswitch");


    // if the checkbox is checked display details if a file has been provided
    display_checkbox.onchange = function() {

        if (display_checkbox.checked){
            if (host_label.value != ""){
                $("#host_details").slideToggle("slow");
            }
            if ((tgt_label.value != "") && (!selected_mode.checked)) {
                $("#tgt_details").slideToggle("slow");
            }
        } else {
            $("#host_details").slideUp();
            $("#tgt_details").slideUp();
        }
    }

    // if a host file is added and the checkbox is checked, display data
    $("#host_file_input").change(function(){
        if (display_checkbox.checked){
            if (host_label.value != ""){
                $("#host_details").slideToggle("slow");
            } else {
                $("#host_details").slideUp();
            }

        }
    });

    // if a target file is added and the checkbox is checked, display data
    $("#tgt_file_input").change(function(){
        if (display_checkbox.checked){
            if (tgt_label.value != ""){
                $("#tgt_details").slideToggle("slow");
            } else {
                $("#tgt_details").slideUp();
            }
        }
    });

};

function getMode() {
    // checked == Extract
    // !checked == Hide
    var selected_mode = document.getElementById("myonoffswitch");

    if (selected_mode.checked) {
        document.getElementById("H_E").value = "Extract";
        console.log("Extract");
        return "Extract";
    } else {
        document.getElementById("H_E").value = "Hide";
        console.log("Hide");
        return "Hide";
    }
};


function isReadyToSubmit() {
    var selected_mode1 = document.getElementById("myonoffswitch");

    if (operation_mode == "Hide") {

    }
    if (operation_mode == "Extract") {

    }

};

function downloadFileAuto() {
    document.getElementById("downloadbutton").click();
};


