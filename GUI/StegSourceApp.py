#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/pysteg
## Copyright Andrew Scott 2016
## Drowned Coast Software


import wx
import os
import urllib
import subprocess
import math
import tempfile
import sys
sys.path.append( os.path.dirname( os.path.dirname( os.path.abspath(__file__) ) ) )
from StegLibs import StegCommon, StegIMG, StegWav



## ----------------------------------------------------------------------------------------------------------
ApplicationOfficialName = "Steg Source"

class MyFileDropTarget(wx.FileDropTarget):
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window
        self.helper = UIHelper()

    def OnDropFiles(self, x, y, filenames):
        self.window.Clear()
        self.window.SetDefaultStyle(wx.TextAttr(wx.BLACK))
        for path in filenames:
            self.window.SetValue("File: " + path)
            self.window.AppendText("\nFile Size: " +  str(self.helper.getFileSize(path)))
            self.window.AppendText("\nFile Type: " + str(path.split('.')[-1]))
            lenSoFar = len(self.window.GetValue())
            if self.window == frm.ImportFileText:
                frm.tgtpath = path
                ## find if filetype is supported
                if self.helper.isFileSupported(path):
                    self.window.AppendText("    SUPPORTED")
                    frm.ValidTargetPresent = True
                    lenAfter = len(self.window.GetValue())
                    self.window.SetStyle(lenSoFar,lenAfter,wx.TextAttr("green",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
                    ## find max file size that can be hidden
                    if self.helper.availableSpaceToHide(path) != 0:
                        self.window.AppendText("\nAvailable Space: " + str(self.helper.availableSpaceToHide(path)))
                else:
                    self.window.AppendText("    NOT SUPPORTED")
                    lenAfter = len(self.window.GetValue())
                    self.window.SetStyle(lenSoFar,lenAfter,wx.TextAttr("red",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
            else:
                frm.hiddenpath = path
                frm.ValidHiddenPresent = True
            frm.CanIRun("1")



class UIHelper:
    def __init__(self):
        pass

    def getFileSizeRaw(self, path):
        return os.stat(path).st_size

    def getFileSize(self, path):
        sz = os.stat(path).st_size
        return self.humanizeFileSize(sz)

    def humanizeFileSize(self, size):
        size = abs(size)
        if (size==0):
            return "0B"
        units = ['Bytes','KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB']
        p = math.floor(math.log(size, 2)/10)
        return "%.3f%s" % (size/math.pow(1024,p),units[int(p)])

    def isFileSupported(self,path):
        fileType = path.split('.')[-1]
        IMGSupport = StegIMG.StegIMGHelper().get_supported_file_types()
        VIDSupport = []
        SOUNDSupport = []
        if (fileType.upper() in IMGSupport):
            return True
        elif (fileType.upper() in VIDSupport):
            return True
        elif (fileType.upper() in SOUNDSupport):
            return True
        else:
            return False

    def availableSpaceToHide(self,path):
        self.spaceToHideRaw = StegIMG.StegIMGHelper().return_max_size(path)
        if self.spaceToHideTaw == 0:
            return 0
        else:
            return self.humanizeFileSize(self.spaceToHideRaw)

    def availableSpaceToHideRaw(self,path):
        self.spaceToHideRaw = StegIMG.StegIMGHelper().return_max_size(path)
        if self.spaceToHideRaw == 0:
            return 0
        else:
            return self.spaceToHideRaw


class OpenImageFromURL:

    def __init__(self,url, allowedTypes):
        self.url = url
        self.tempDirectory = str(tempfile.gettempdir()) + "/"
        print self.tempDirectory
        self.imageType = url.split('.')[::-1][0]
        self.allowedTypes = allowedTypes
        self.tempImageList = []

    def DownloadImage(self):
        ## if the file is one of the accepted types, download the image
        if self.url[:4] != "http":
            print "invalid URL"
            return False
        if (self.imageType.upper() in self.allowedTypes) or (self.allowedTypes == "*"):
            self.urlImageName = self.url.split('/')[::-1][0]
            self.tempImagePath = self.tempDirectory + self.urlImageName
            try:
                urllib.urlretrieve(self.url, self.tempImagePath)
                self.imageExistsInTemp = True
                self.tempImageList.append(self.tempImagePath)
                return True
            except Exception, e:
                return False

    def DestroyTempImage(self):
        try:
            for tmpimg in self.tempImageList:
                try:
                    subprocess.check_call(["rm", tmpimg])
                except Exception, e:
                    return False
            return True
        except Exception, e:
            return False



class MyFrame(wx.Frame):
    ## init all basic UI and starting values
    def __init__(self):

        ## defaults --------------------------------------------------------------------------------
        self.EncryptionEnabled = False
        self.EncryptionModesVisibleKey = False
        self.ValidTargetPresent = False
        self.ValidHiddenPresent = False
        # Create frame --------------------------------------------------------------------------------
        wx.Frame.__init__(self, None, title=ApplicationOfficialName ,size=(600,450))
        self.p = wx.Panel(self)
        self.helper = UIHelper()

        # Create menu bar --------------------------------------------------------------------------------
        menubar = wx.MenuBar()
        menubar.SetName('Steg')
        helpMenu = wx.Menu()
        helpMenu.Append(wx.ID_HELP,'&Help\tCtrl+H')
        helpMenu.Append(wx.ID_ABOUT, "&About\tShift+Ctrl+A")
        menubar.Append(helpMenu, 'Help')
        wx.EVT_MENU(self, wx.ID_HELP, self.openHelp)
        wx.EVT_MENU(self, wx.ID_ABOUT, self.openAbout)
        self.SetMenuBar(menubar)

        ## Create Box sizer --------------------------------------------------------------------------------
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        ## Select Mode --------------------------------------------------------------------------------
        ModeLabel = wx.StaticText(self.p, -1, "Select Operation Mode:   ")
        availableModes = {'--h':'Hide','--e':'Extract'}
        self.friendlyModeList = []
        for key in availableModes:
            self.friendlyModeList.append(availableModes[key])
        self.ModePicker = wx.Choice(self.p, -1, (85, 18), choices=self.friendlyModeList)
        #self.Bind(wx.EVT_CHOICE,self.ModePicked,self.ModePicker)
        ModeSizer = wx.BoxSizer(wx.HORIZONTAL)
        ModeSizer.Add(ModeLabel,0,wx.LEFT,2)
        ModeSizer.Add(self.ModePicker,1,wx.RIGHT,2)

        self.sizer.Add(ModeSizer,0,wx.ALL,5)

        ## Target File import --------------------------------------------------------------------------------
        ImportFileLabel = wx.StaticText(self.p, -1, "Import Target File:")
        ImportFileButton = wx.Button(self.p, -1, label="Browse")
        ImportFileDownloadButton = wx.Button(self.p, -1, label="Download")
        self.ImportFileText = wx.TextCtrl(self.p, -1,style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_RICH)
        self.ImportFileText.SetDefaultStyle(wx.TextAttr(wx.LIGHT_GREY))
        self.ImportFileText.AppendText("Drop Target File Here")
        ImportFileSizer = wx.BoxSizer(wx.HORIZONTAL)
        ImportFileSizer.Add(ImportFileLabel, 0, wx.RIGHT, 2)
        ImportFileSizer.Add(ImportFileButton,1, wx.LEFT,2)
        ImportFileSizer.Add(ImportFileDownloadButton,1,wx.LEFT,2)

        self.sizer.Add(ImportFileSizer, 0, wx.ALL, 5)
        self.sizer.Add(self.ImportFileText, 2, wx.ALL|wx.EXPAND, 5)
        self.Bind(wx.EVT_BUTTON, self.onOpenFileTarget, ImportFileButton)
        self.Bind(wx.EVT_BUTTON, self.DownloadFileTarget, ImportFileDownloadButton)

        dtTarget = MyFileDropTarget(self.ImportFileText)
        self.ImportFileText.SetDropTarget(dtTarget)


        ## Hidden File import --------------------------------------------------------------------------------
        self.HiddenFileLabel = wx.StaticText(self.p, -1, "Import File to Hide:")
        self.HiddenFileButton = wx.Button(self.p, -1, label="Browse")
        self.HiddenFileDownloadButton = wx.Button(self.p, -1, label="Download")
        self.HiddenFileText = wx.TextCtrl(self.p, -1,style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_RICH)
        self.HiddenFileText.SetDefaultStyle(wx.TextAttr(wx.LIGHT_GREY))
        self.HiddenFileText.AppendText("Drop File to Hide Here")
        HiddenFileSizer = wx.BoxSizer(wx.HORIZONTAL)
        HiddenFileSizer.Add(self.HiddenFileLabel, 0, wx.RIGHT, 2)
        HiddenFileSizer.Add(self.HiddenFileButton,1, wx.LEFT,2)
        HiddenFileSizer.Add(self.HiddenFileDownloadButton,1,wx.LEFT,2)

        self.sizer.Add(HiddenFileSizer, 0, wx.ALL, 5)
        self.sizer.Add(self.HiddenFileText, 2, wx.ALL|wx.EXPAND, 5)
        self.Bind(wx.EVT_BUTTON, self.onOpenFileHidden,self.HiddenFileButton)
        self.Bind(wx.EVT_BUTTON, self.DownloadFileHidden, self.HiddenFileDownloadButton)

        dtHidden = MyFileDropTarget(self.HiddenFileText)
        self.HiddenFileText.SetDropTarget(dtHidden)

        ## Encryption --------------------------------------------------------------------------------
        EncryptionKeyLabel = wx.StaticText(self.p, -1, "Enter Encryption Key (optional): ")
        self.EncryptionKeyValue = wx.TextCtrl(self.p, -1, "",size=(200,30),style=wx.TE_PASSWORD)

        EncryptionSizer = wx.BoxSizer(wx.HORIZONTAL)
        EncryptionSizer.Add(EncryptionKeyLabel,0,wx.LEFT,2)
        EncryptionSizer.Add(self.EncryptionKeyValue,1,wx.RIGHT,2)
        self.EncryptionKeyValue.Bind(wx.EVT_TEXT, self.OnPassword,self.EncryptionKeyValue)
        self.sizer.Add(EncryptionSizer,1,wx.RIGHT,5)


        self.EncryptionModeLabel = wx.StaticText(self.p, -1, "Select Encryption Mode: ")
        self.supportedEncryptionModes = ['AES','ARC4']
        self.EncryptionModePicker = wx.Choice(self.p, -1, (85, 18), choices=self.supportedEncryptionModes)
        self.EncryptionSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.EncryptionSizer2.Add(self.EncryptionModeLabel,0,wx.LEFT,2)
        self.EncryptionSizer2.Add(self.EncryptionModePicker,0,wx.RIGHT,2)
        self.sizer.Add(self.EncryptionSizer2,1,wx.RIGHT,5)
        self.EncryptionSizer2.Hide(0)
        self.EncryptionSizer2.Hide(1)


        ## Run it -----------------------------------------------------------------------------------
        self.RunItButton = wx.Button(self.p,-1,size=(100,20),label="Execute")
        ButtonsSizer = wx.GridBagSizer(1,4)
        ButtonsSizer.Add(self.RunItButton,pos=(0,34),flag=wx.ALL|wx.ALIGN_RIGHT,border=2)
        self.sizer.Add(ButtonsSizer,0,wx.ALL|wx.EXPAND,5)
        self.RunItButton.Disable()
        self.Bind(wx.EVT_BUTTON, self.RunEverything, self.RunItButton)

        self.Bind(wx.EVT_CHOICE, self.CanIRun, self.ModePicker)
        self.Bind(wx.EVT_CHOICE, self.CanIRun, self.EncryptionModePicker)

        ## Wrap up UI loading
        self.ModePicked(wx._core.CommandEvent)
        self.p.SetSizer(self.sizer)



    ## ----------------------------------------------------------------------------------------------
    ## Logic for UI and backend


    ## Quit application
    def OnQuit(self, e):
        self.Close()

    ## Called when Hide/Extract is Toggled
    def ModePicked(self, e):
        if self.friendlyModeList[self.ModePicker.GetCurrentSelection()] == "Extract":
            self.HiddenFileButton.Disable()
            self.HiddenFileDownloadButton.Disable()
            self.HiddenFileText.Clear()
            self.HiddenFileText.SetDefaultStyle(wx.TextAttr(wx.LIGHT_GREY))
            self.HiddenFileText.SetValue("")
            self.HiddenFileText.Disable()
        else:
            self.HiddenFileButton.Enable()
            self.HiddenFileDownloadButton.Enable()
            self.HiddenFileText.Enable() 
            self.HiddenFileText.SetDefaultStyle(wx.TextAttr(wx.LIGHT_GREY))
            if self.HiddenFileText.GetValue() == "":
                self.HiddenFileText.AppendText("Drop File to Hide Here")

    ## Called for every keystroke in Password field
    def OnPassword(self, e):
        if not self.EncryptionKeyValue.IsEmpty():
            self.EncryptionEnabled = True
            self.EncryptionModesVisible()
        else:
            self.EncryptionEnabled = False
            self.EncryptionModesVisible()

    ## Called when a password is not ""
    def EncryptionModesVisible(self):
        ## If encryption enabled
        if self.EncryptionEnabled and not self.EncryptionModesVisibleKey:
            self.EncryptionModesVisibleKey = True
            self.EncryptionSizer2.Show(0)
            self.EncryptionSizer2.Show(1)
            self.sizer.Layout()
        elif not self.EncryptionEnabled and self.EncryptionModesVisibleKey:
            self.EncryptionSizer2.Hide(0)
            self.EncryptionSizer2.Hide(1)
            self.sizer.Layout()
            self.EncryptionModesVisibleKey = False
        self.CanIRun("1")

    ## Open file dialog and choose target file
    ## Also completes UI and displays file size and whether supported
    def onOpenFileTarget(self, e):
        dlg = wx.FileDialog(
            self, message="Choose a target file",
            defaultFile="",
            wildcard='*',
            style=wx.OPEN | wx.CHANGE_DIR
            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            for path in paths:
                self.ImportFileText.Clear()
                self.ImportFileText.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                self.ImportFileText.SetValue("File: " + path)
                self.tgtpath = path
                self.ImportFileText.AppendText("\nFile Size: " +  str(self.helper.getFileSize(self.tgtpath)))
                self.ImportFileText.AppendText("\nFile Type: " + str(path.split('.')[-1]))
                lenSoFar = len(self.ImportFileText.GetValue())
                if self.helper.isFileSupported(path):
                    self.ImportFileText.AppendText("    SUPPORTED")
                    self.ValidTargetPresent = True
                    lenAfter = len(self.ImportFileText.GetValue())
                    self.ImportFileText.SetStyle(lenSoFar,lenAfter,wx.TextAttr("green",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
                    if self.helper.availableSpaceToHide(path) != 0:
                        self.ImportFileText.AppendText("\nAvailable Space: " + str(self.helper.availableSpaceToHide(self.tgtpath)))
                else:
                    self.ImportFileText.AppendText("    NOT SUPPORTED")
                    lenAfter = len(self.ImportFileText.GetValue())
                    self.ImportFileText.SetStyle(lenSoFar,lenAfter,wx.TextAttr("red",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
        self.CanIRun("1")
        dlg.Destroy()

    ## Opens file dialog to choose the file that will be hidden
    ## Also completes UI and displays file size
    def onOpenFileHidden(self, e):
        self.HiddenFileText.Clear()
        dlg = wx.FileDialog(
            self, message="Choose a file to hide",
            defaultFile="",
            wildcard='*',
            style=wx.OPEN | wx.CHANGE_DIR
            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            for path in paths:
                self.HiddenFileText.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                self.HiddenFileText.SetValue("File: " + path)
                self.hiddenpath = path
                self.ValidHiddenPresent = True
                self.HiddenFileText.AppendText("\nFile Size: " +  str(self.helper.getFileSize(self.hiddenpath)))
                self.HiddenFileText.AppendText("\nFile Type: " + str(path.split('.')[-1]))
        self.CanIRun("1")
        dlg.Destroy()
        dlg.Destroy()

    ## Allows user to enter a URL to download a target file
    ## Also completes UI and file size/ support
    def DownloadFileTarget(self, e):
        self.DownloadUrlTarget = self.showMessageDlgDownload('Enter URL to download Target File', "Download Target from URL",
                            wx.OK|wx.CANCEL)
        if self.DownloadUrlTarget != "":
            self.TargetURLOpener = OpenImageFromURL(self.DownloadUrlTarget, StegIMG.StegIMGHelper().get_supported_file_types())
            if self.TargetURLOpener.DownloadImage():
                self.ImportFileText.Clear()
                self.ImportFileText.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                self.ImportFileText.SetValue("File: " + self.TargetURLOpener.urlImageName)
                self.tgtpath = self.TargetURLOpener.tempImagePath
                self.ImportFileText.AppendText("\nFile Size: " +  str(self.helper.getFileSize(self.tgtpath)))
                self.ImportFileText.AppendText("\nFile Type: " + str(self.tgtpath.split('.')[-1]))
                lenSoFar = len(self.ImportFileText.GetValue())
                if self.helper.isFileSupported(self.tgtpath):
                    self.ImportFileText.AppendText("    SUPPORTED")
                    self.ValidTargetPresent = True
                    lenAfter = len(self.ImportFileText.GetValue())
                    self.ImportFileText.SetStyle(lenSoFar,lenAfter,wx.TextAttr("green",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
                    if self.helper.availableSpaceToHide(self.tgtpath) != 0:
                        self.ImportFileText.AppendText("\nAvailable Space: " + str(self.helper.availableSpaceToHide(self.tgtpath)))
                else:
                    self.ImportFileText.AppendText("    NOT SUPPORTED")
                    lenAfter = len(self.ImportFileText.GetValue())
                    self.ImportFileText.SetStyle(lenSoFar,lenAfter,wx.TextAttr("red",colBack=wx.NullColour, font=wx.NullFont, alignment=wx.TEXT_ALIGNMENT_DEFAULT))
            else:
                ## download returned false
                self.showMessageDlg("Download Failed\n Check that URL is valid", "Download Failed", wx.OK|wx.ICON_EXCLAMATION)
        else:
            print "nothing returned"
        self.CanIRun("1")

    ## Actually supports the message dialog for downloading URLs
    def showMessageDlgDownload(self, msg, title, style):
        dlg = wx.TextEntryDialog(parent=None, message=msg, caption=title, style=style)
        dlg.ShowModal()
        if (dlg.GetValue() != ""):
            return dlg.GetValue()
        else:
            return ""
        dlg.Destroy()

    ## Allows user to enter a URL to download a file to hide
    def DownloadFileHidden(self, e):
        self.DownloadUrlHidden = self.showMessageDlgDownload('Enter URL to download Hidden File', "Download Hidden from URL",
                            wx.OK|wx.CANCEL)
        if self.DownloadUrlHidden != "":
            self.HiddenURLOpener = OpenImageFromURL(self.DownloadUrlHidden,"*")
            if self.HiddenURLOpener.DownloadImage():
                self.HiddenFileText.Clear()
                self.HiddenFileText.SetDefaultStyle(wx.TextAttr(wx.BLACK))
                self.HiddenFileText.SetValue("File: " + self.HiddenURLOpener.urlImageName)
                self.hiddenpath = self.HiddenURLOpener.tempImagePath
                self.ValidHiddenPresent = True
                self.HiddenFileText.AppendText("\nFile Size: " +  str(self.helper.getFileSize(self.hiddenpath)))
                self.HiddenFileText.AppendText("\nFile Type: " + str(self.hiddenpath.split('.')[-1]))
            else:
                ## download returned false
                self.showMessageDlg("Download Failed\n Check that URL is valid", "Download Failed", wx.OK|wx.ICON_EXCLAMATION)
        self.CanIRun(1)

    # Runs on many events, helps decide if the App is ready to run under different circumstances
    def CanIRun(self, e):
        self.ModePicked("1")
        print "Event"
        if self.friendlyModeList[self.ModePicker.GetCurrentSelection()] == "Extract":
            self.OperationMode = "Extract"
            if self.ValidTargetPresent == True:
                if self.helper.isFileSupported(self.tgtpath):
                    if self.EncryptionEnabled:
                        self.EncryptionKey = self.EncryptionKeyValue.GetValue()
                        self.EncryptionAlgorithmChoice = self.EncryptionModePicker.GetSelection()
                        print "%s : %s" % (self.EncryptionKey, self.EncryptionAlgorithmChoice)
                    else:
                        self.EncryptionKey = None
                        self.EncryptionAlgorithmChoice = None
                    self.RunItButton.Enable()
                else:
                    self.RunItButton.Disable()
            else:
                self.RunItButton.Disable()
        elif self.friendlyModeList[self.ModePicker.GetCurrentSelection()] == "Hide":
            self.OperationMode = "Hide"
            if (self.ValidHiddenPresent == True) and (self.ValidTargetPresent == True):
                if self.helper.isFileSupported(self.tgtpath):
                    if self.helper.availableSpaceToHideRaw(self.tgtpath) >= self.helper.getFileSizeRaw(self.hiddenpath):
                        print str(self.helper.availableSpaceToHideRaw(self.tgtpath))
                        print str(self.helper.getFileSizeRaw(self.hiddenpath))
                        if self.EncryptionEnabled:
                            self.EncryptionKey = self.EncryptionKeyValue.GetValue()
                            self.EncryptionAlgorithmChoice = self.EncryptionModePicker.GetSelection()
                            print "%s : %s" % (self.EncryptionKey, self.EncryptionAlgorithmChoice)
                        else:
                            self.EncryptionKey = None
                            self.EncryptionAlgorithmChoice = None
                        self.RunItButton.Enable()
                    else:
                        self.showMessageDlg("File to hide is too large\nSpace Available: %s\nFile to hide: %s" % (str(self.helper.availableSpaceToHide(self.tgtpath)),str(self.helper.getFileSize(self.hiddenpath))),"File is too large", wx.OK|wx.ICON_EXCLAMATION)
                        self.RunItButton.Disable()
                else:
                    self.RunItButton.Disable()
            else:
                self.RunItButton.Disable()
        else:
            self.RunItButton.Disable()

    ## Init a Steg*** Class and run the process
    def RunEverything(self, e):
        if self.friendlyModeList[self.ModePicker.GetCurrentSelection()] == "Extract":
            try:
                if self.EncryptionAlgorithmChoice != None:
                    S = StegIMG.StegIMG(None, self.tgtpath, self.EncryptionKey,self.supportedEncryptionModes[self.EncryptionAlgorithmChoice])
                else:
                    S = StegIMG.StegIMG(None, self.tgtpath, self.EncryptionKey)
                S.open_image(self.tgtpath)
                if S.imageType.lower() == 'png':
                    S.use_correct_function_extract(S.fg)
                elif (S.imageType.lower() == 'tif') | (S.imageType.lower() == 'tiff'):
                    S.use_correct_function_extract(S.fg)
                elif (S.imageType.lower() == 'bmp'):
                    S.use_correct_function_extract(S.fg)
                self.ExtractWasSuccessful(S.ExtractedFileName)
            except Exception, e:
                print str(e)
                self.ExtractFailed(str(e))
        else:
            try:
                if self.EncryptionAlgorithmChoice != None:
                    S = StegIMG.StegIMG(self.hiddenpath, self.tgtpath, self.EncryptionKey,self.supportedEncryptionModes[self.EncryptionAlgorithmChoice])
                else:
                    S = StegIMG.StegIMG(None, self.tgtpath, self.EncryptionKey)
                S.open_image(self.tgtpath)
                if S.imageType.lower() == 'png':
                    S.use_correct_function_hide(S.fg, self.hiddenpath, 'png')
                elif (S.imageType.lower() == 'tif') | (S.imageType.lower() == 'tiff'):
                    S.use_correct_function_hide(S.fg, self.hiddenpath, 'TIFF')
                elif S.imageType.lower() == 'bmp':
                    S.use_correct_function_hide(S.fg, self.hiddenpath, 'bmp')
                self.HideWasSuccessful(S.NewFileWithHidden)
            except Exception, e:
                print str(e)
                self.HideFailed(str(e))

    ## Dialog for success
    def showMessageDlg(self, msg, title, style):
        """"""
        dlg = wx.MessageDialog(parent=None, message=msg,
                               caption=title, style=style)
        dlg.ShowModal()
        dlg.Destroy()

    def HideWasSuccessful(self,filename):
        self.showMessageDlg("New file: " + filename, "Hide Successful!", wx.OK)

    def ExtractWasSuccessful(self,filename):
        self.showMessageDlg("Extracted file: " + filename, "Extraction Successful!", wx.OK)

    ## Dialog for failure
    def HideFailed(self,exception):
        self.showMessageDlg("Hide Failed\n Exception: "+ exception, "Hide Failed", wx.OK|wx.ICON_EXCLAMATION)

    def ExtractFailed(self,exception):
        self.showMessageDlg("Extraction Failed\nExecption: " + exception, "Extraction Failed", wx.OK|wx.ICON_EXCLAMATION)

    ## Help Dialog
    def openHelp(self,e):
        print "open Help"
        pass

    ## About Dialog
    def openAbout(self,e):

        supportedFileFormats = StegIMG.StegIMGHelper().get_supported_file_types()
        description = """%s is a GUI application based on the open source pysteg project which aims
        to enable users to hide messages and files within a variety of different mediums.

        %s currently supports the hiding within the following file formats:
        %s
        """ % (ApplicationOfficialName,ApplicationOfficialName,str(supportedFileFormats))

        licence = """%s is free software; you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

%s is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details. You should have
received a copy of the GNU General Public License along with %s;
if not, write to the Free Software Foundation, Inc., 59 Temple Place,
Suite 330, Boston, MA  02111-1307  USA



See source code at: https://bitbucket.org/beatsbears/pysteg""" % (ApplicationOfficialName, ApplicationOfficialName, ApplicationOfficialName)


        info = wx.AboutDialogInfo()

        #info.SetIcon(wx.Icon('hunter.png', wx.BITMAP_TYPE_PNG))
        info.SetName(ApplicationOfficialName)
        info.SetVersion('0.1')
        info.SetDescription(description)
        info.SetCopyright('(C) 2016 Andrew Scott')
        info.SetWebSite('http://drownedcoast.com')
        info.SetLicence(licence)
        info.AddDeveloper('Andrew Scott')

        wx.AboutBox(info)


app = wx.PySimpleApp()
app.SetAppName(ApplicationOfficialName)
frm = MyFrame()
frm.Show()
app.MainLoop()



