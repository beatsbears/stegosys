
# stegosys #

Python based steganography tool for hiding files within different media formats. GUI and CL tools.

## Currently Supported mediums ##
* PNG
* BMP
* TIFF
* ICO
* WAV
* AIFF

## Plans to add support for ##
* MP3
* GIF
* TBD

### Author ###

andrew.scott<at>drownedcoast.xyz

