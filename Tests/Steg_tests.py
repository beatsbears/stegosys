#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software

import unittest
import sys
import os
import binascii
sys.path.append( os.path.dirname( os.path.dirname( os.path.abspath(__file__) ) ) )
from StegLibs import StegCommon, StegWav, StegIMG, StegAIF

class CommonTests(unittest.TestCase):

    def setUp(self):
        # setup with text file
        txt = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfile.txt'
        self.SCtxt = StegCommon.StegCommon(txt, 'password', 'AES')

    # test retreiving correct file type
    def test_getFileType(self):
        self.assertEquals(self.SCtxt.fileType, 'TXT', 'Sample file is not a .txt')

    # test MD5 hash generation
    def test_MD5(self):
        self.assertEquals(self.SCtxt.return_MD5(self.SCtxt.HostFile), '97f9025a596ba867c85618a6204e3c6b', 'Hash does not match known value')

    # try encrypting a simple string and hex encoding
    def test_encrypttxtAES(self):
        plain = 'hello, world!'
        self.assertEquals(binascii.b2a_hex(self.SCtxt.encrypt_message(plain, self.SCtxt.password)), 'd301fe92a241120b4ac9c32185', 'Encryption does not match known value')

    def test_encrypttxtARC4(self):
        plain = 'hello, world!'
        self.SCtxt.encryptionMode = 'ARC4'
        self.assertEquals(binascii.b2a_hex(self.SCtxt.encrypt_message(plain, self.SCtxt.password)), '08a3233cdd1dde918ed8e52be9', 'Encryption does not match known value')

    # test the human readable method, this is just for user visibility
    def test_humanSize(self):
        self.assertEquals(self.SCtxt.humanize_file_size(100000), '97.656KiB', 'Error converting to human readable format')
        self.assertEquals(self.SCtxt.humanize_file_size(100000000), '95.367MiB', 'Error converting to human readable format')
        self.assertEquals(self.SCtxt.humanize_file_size(100000000000), '93.132GiB', 'Error converting to human readable format')
        self.assertEquals(self.SCtxt.humanize_file_size(100000000000000), '90.949TiB', 'Error converting to human readable format')
        self.assertEquals(self.SCtxt.humanize_file_size(100), '100.000Bytes', 'Error converting to human readable format')

    # test the text to binary encoding against a known correct value
    def test_textToBinary(self):

        txt = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfile.txt'

        # precomputed file with ascii representation of binary
        bin = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfilebin.txt'
        binString = open(bin,'r').readline()

        # don't bother encrypting
        self.SCtxt.encryptionEnabled = False
        self.assertEquals(self.SCtxt.text_to_binary(txt, 3000), binString, 'Text to binary conversion failed to match known value')

    # def test_reconstituteFromBinary(self):
    #     # precomputed file with ascii representation of binary
    #     bin = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfilebin.txt'
    #     binString = open(bin,'r').readline()
    #
    #     #self.SCtxt.verbose = True
    #     self.SCtxt.encryptionEnabled = False
    #     self.SCtxt.reconstitute_from_binary(binString)
    #     pass

    # test changing the final bit
    def test_setbit(self):
        self.assertEquals(self.SCtxt.set_bit(255, '0'), 254, 'Setbit error')
        self.assertEquals(self.SCtxt.set_bit(176, '1'), 177, 'Setbit error')

class StegIMGTests(unittest.TestCase):

    def setUp(self):
        txt = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfile.txt'
        pngLocation = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/sample.png'
        tiffLocation= os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/sample.tiff'
        bmpLocation= os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/sample.bmp'

        self.SIpng = StegIMG.StegIMG(txt,pngLocation,None,'ARC4',False)
        self.SItiff = StegIMG.StegIMG(txt,tiffLocation,None,'ARC4',False)
        self.SIbmp = StegIMG.StegIMG(txt,bmpLocation,None,'ARC4',False)
        self.SH = StegIMG.StegIMGHelper()

    # test opening image files
    def test_imageOpen(self):
        # open png
        self.SIpng.open_image(self.SIpng.imageToUse)
        self.assertEquals(self.SIpng.imageMode,'RGBA','Failed to read image mode or open image file')

        # open tiff
        self.SItiff.open_image(self.SItiff.imageToUse)
        self.assertEquals(self.SItiff.imageMode,'RGB','Failed to read image mode or open image file')

        # open bmp
        self.SIbmp.open_image(self.SIbmp.imageToUse)
        self.assertEquals(self.SIbmp.imageMode,'L','Failed to read image mode or open image file')

    def test_assignOutputFileType(self):
        # test bmp
        self.assertEquals(self.SIpng.assign_output_file_type('bmp'), 'bmp', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('BMP'), 'bmp', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('bMp'), 'bmp', 'Failed to assign output file type or did not recognize file type')
        self.assertNotEquals(self.SIpng.assign_output_file_type('bpm'), 'bmp', 'Failed to assign output file type or did not recognize file type')

        # test png
        self.assertEquals(self.SIpng.assign_output_file_type('png'), 'png', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('PNG'), 'png', 'Failed to assign output file type or did not recognize file type')

        # test tiff
        self.assertEquals(self.SIpng.assign_output_file_type('tiff'), 'TIFF', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('TIFF'), 'TIFF', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('Tif'), 'TIFF', 'Failed to assign output file type or did not recognize file type')
        self.assertEquals(self.SIpng.assign_output_file_type('TIF'), 'TIFF', 'Failed to assign output file type or did not recognize file type')

    # test get supported images
    def test_getSupportedFileTypes(self):
        self.assertListEqual(self.SH.get_supported_file_types(), ['PNG', 'TIFF', 'TIF', 'BMP', 'ICO'], 'Supported file types don\'t match supplied list')

    # test supported files
    def test_isFileSupported(self):
        self.SH.verbose = False
        self.assertTrue(self.SH.is_file_supported('TIFF'), 'Supported file types don\'t match supplied list')
        self.assertTrue(self.SH.is_file_supported('PNG'), 'Supported file types don\'t match supplied list')
        self.assertTrue(self.SH.is_file_supported('BMP'), 'Supported file types don\'t match supplied list')
        self.assertTrue(self.SH.is_file_supported('TIF'), 'Supported file types don\'t match supplied list')
        self.assertFalse(self.SH.is_file_supported('SVG'), 'Supported file types don\'t match supplied list')

    # test max file size
    def test_returnMaxSize(self):
        self.assertGreater(self.SH.return_max_size(self.SIpng.imageToUse), 10000, 'File size calculation error')


class StegWavTests(unittest.TestCase):

    def setUp(self):
        txt = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfile.txt'
        wavLocation= os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/sample.wav'

        self.SW = StegWav.StegWav(txt,wavLocation,None,'ARC4',False)
        self.SH = StegWav.StegWAVHelper()

    # test opening a wav file and getting attributes
    def test_openAudio(self):
        self.SW._open_audio(self.SW.audioToUse)
        self.assertEquals(self.SW.audioType,'wav')
        self.assertEquals(self.SW.compressionType,'NONE')

    # get supported file type list
    def test_getSupportedFileTypes(self):
        self.assertListEqual(self.SH.get_supported_file_types(),['WAV', 'WAVE'],'Supported file types don\'t match supplied list')

    # test supported files
    def test_isFileSupported(self):
        self.SH.verbose = False
        self.assertTrue(self.SH.is_file_supported('WAV'), 'Supported file types don\'t match supplied list')
        self.assertTrue(self.SH.is_file_supported('WAVE'), 'Supported file types don\'t match supplied list')
        self.assertFalse(self.SH.is_file_supported('mp3'), 'Supported file types don\'t match supplied list')

    def test_returnMaxSize(self):
        self.assertGreater(self.SH.return_max_size(self.SW.audioToUse),10000)



class StegAifTests(unittest.TestCase):
    def setUp(self):
        txt = os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/testfile.txt'
        aifLocation= os.path.dirname(os.path.abspath(__file__)) + '/SampleFiles/sample.aiff'

        self.SA = StegAIF.StegAIF(txt,aifLocation,None,'ARC4',False)
        self.SH = StegAIF.StegAIFHelper()

    # test opening a wav file and getting attributes
    def test_openAudio(self):
        self.SA._open_audio(self.SA.audioToUse)
        self.assertEquals(self.SA.audioType,'aiff')
        self.assertEquals(self.SA.compressionType,'NONE')

    # get supported file type list
    def test_getSupportedFileTypes(self):
        self.assertListEqual(self.SH.get_supported_file_types(),['AIF', 'AIFF'],'Supported file types don\'t match supplied list')

    # test supported files
    def test_isFileSupported(self):
        self.SH.verbose = False
        self.assertTrue(self.SH.is_file_supported('AIF'), 'Supported file types don\'t match supplied list')
        self.assertTrue(self.SH.is_file_supported('AIFF'), 'Supported file types don\'t match supplied list')
        self.assertFalse(self.SH.is_file_supported('wav'), 'Supported file types don\'t match supplied list')

    def test_returnMaxSize(self):
        self.assertGreater(self.SH.return_max_size(self.SA.audioToUse),10000)


if __name__ == '__main__':
    unittest.main()