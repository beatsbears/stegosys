#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software


import binascii
from Crypto.Cipher import ARC4,AES
from Crypto.Hash import SHA256,MD5
import random
import math
import logging

import StegAIF
import StegWav
import StegIMG

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('stegosys.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class StegCommon:
    ''' Provides common functions for many stegosys classes. Primary function is to convert
        files into workable binary and provide cryptographic support.
    '''

    def __init__(self, filePath, password=None, encryptionMode="ARC4",verbose=False):
        # name some constants
        self.bufferSignature = 'EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE'
        self.bufferSignature2 = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'
        self.preMD5 = ''
        self.postMD5 = ''
        self.HostFile = filePath
        self.password = password
        self.encryptionMode = encryptionMode
        self.verbose = verbose

        # get image type from image path
        if filePath != None:
            self.fileType = (filePath.split('.')[-1]).upper()

        # determine if encryption will be used
        if self.password != None:
            self.encryptionEnabled = True
        else:
            self.encryptionEnabled = False


        # return MD5 hash for checks
    def return_MD5(self, filePath):
        changeHash = MD5.new()
        changeHash.update(filePath)
        return changeHash.hexdigest()


        # encrypts data with desired cipher suite
    def encrypt_message(self, plainText, key):
        logger.debug('Encryption Mode: {}'.format(self.encryptionMode))
        if self.encryptionMode == "ARC4":
            try:
                hasher = SHA256.new()
                hasher.update(key)
                tempKey = hasher.hexdigest()
                obj1 = ARC4.new(tempKey)
                cipherText = obj1.encrypt(plainText)
                if self.verbose:
                    print '[+] Message successfully encrypted'
                    logger.debug('Message successfully encrypted')
                return cipherText
            except Exception as e:
                print '\033[0;31m[!] Encryption failed\033[0m'
                logger.error('Encryption failed: {}'.format(str(e)))
                print str(e)
        elif self.encryptionMode == "AES":
            try:
                hasher = SHA256.new()
                hasher.update(key)
                tempKey = hasher.hexdigest()[:32]
                obj1 = AES.new(tempKey,AES.MODE_CFB,tempKey[::-1][:16])
                cipherText = obj1.encrypt(plainText)
                if self.verbose:
                    print '[+] Message successfully encrypted'
                    logger.debug('Message successfully encrypted')
                return cipherText
            except Exception as e:
                print '\033[0;31m[!] Encryption failed\033[0m'
                logger.error('Encryption failed: {}'.format(str(e)))
                print str(e)
        else:
            return plainText

    # decrypts data with desired cipher suite
    def decrypt_message(self, cipherText, key):
        logger.debug('Encryption Mode: {}'.format(self.encryptionMode))
        if self.encryptionMode == "ARC4":
            try:
                hasher = SHA256.new()
                hasher.update(key)
                tempKey = hasher.hexdigest()
                obj2 = ARC4.new(tempKey)
                plainText =  obj2.decrypt(cipherText)
                if self.verbose:
                    print '[+] Decryption successful'
                    logger.debug('Decryption successful')
                return plainText
            except Exception as e:
                print '\033[0;31m[!] Decryption failed\033[0m'
                logger.error('Decryption Failed!! {}'.format(str(e)))
                print str(e)
        elif self.encryptionMode == "AES":
            try:
                hasher = SHA256.new()
                hasher.update(key)
                tempKey = hasher.hexdigest()[:32]
                obj2 = AES.new(tempKey,AES.MODE_CFB,tempKey[::-1][:16])
                plainText =  obj2.decrypt(cipherText)
                if self.verbose:
                    print '[+] Decryption successful'
                    logger.debug('Decryption successful')
                return plainText
            except Exception as e:
                print '\033[0;31m[!] Decryption failed\033[0m'
                logger.error('Decryption Failed!! {}'.format(str(e)))
                print str(e)
        else:
            return cipherText



    # Converts contents of file to binary data
    def text_to_binary(self, filecontents, maxsize):
        # open file and add some buffers so we can find our place later and not fuck it up
        try:
            if self.verbose:
                print filecontents
            textFile = open(filecontents, 'rb')
            hiddenFile = filecontents.split('.')[-1]
        except Exception as e:
            print '[!] Failed to open target file'
            logger.error('Failed to open target file: {}'.format(str(e)))
            print str(e)
        try:
            # read data from file and encrypt if necessary
            text = textFile.read()
            text += '\t'
            if self.encryptionEnabled:
                text = self.encrypt_message(text, self.password)

            # add buffer for the end of the actual message
            text += self.bufferSignature
            text += '\t'

            # append the original file type
            text += (hiddenFile).encode('UTF-8')
            text += '\t'

            # add a second buffer
            text += self.bufferSignature2
            textFile.close()

            if self.verbose:
                print '[+] Content: %s' % text
            # convert ascii to hex
            hexText = binascii.b2a_hex(text)

            if self.verbose:
                print '[+] Hex encoded: %s' % hexText
            # convert hex to binary and fill the rest of the bitstream with random hex
            b = ''
            for ch in hexText:
                tmp = bin(ord(ch))[2:]
                if len(tmp) < 7:
                    for i in range(0,(7-len(tmp))):
                        tmp = '0' + tmp
                b += tmp
            for n in range(0,(maxsize - len(b)),7):
                b += str(bin(ord(random.choice('abcdef')))[2:])
            logger.debug('Text to binary operation successful')
            return b
        except Exception as e:
            logger.error('Text to binary conversion failed! {}'.format(str(e)))


    # takes a byte and changes the last bit to whatever we need
    def set_bit(self, oldbyte, newbit):
        lst = list(bin(oldbyte))
        lst[-1] = newbit
        return int(''.join(lst),2)

    # this is a helper function to make file sizes readable for humans
    def humanize_file_size(self, size):
        size = abs(size)
        if (size==0):
            return "0B"
        units = ['Bytes','KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB']
        p = math.floor(math.log(size, 2)/10)
        return "%.3f%s" % (size/math.pow(1024,p),units[int(p)])

    def get_file_extension_host(self):
        return (self.HostFile.split('.')[-1]).upper()


    def reconstitute_from_binary(self, rawbits, location):
        hidden = rawbits
        if location == None:
            location = ""
        try:
            logger.debug('Starting to assemble message from binary')
            # break long string into array for bytes
            b = [hidden[i:i+7] for i in range(0, len(hidden), 7)]
            # convert to string
            c = ''
            for i in b:
                c += chr(int(i,2))
            # if the string length is not even, add a digit
            if len(c) % 2 != 0:
                c += 'A'
            if self.verbose:
                print '\n[+] Extracted Hex snippet: %s' % c[:200]
            # convert back to ascii
            d = binascii.a2b_hex(c[:-10])
            # check to see if the buffer is intact still
            bufferIndex = d.find(self.bufferSignature)
            bufferIndex2 = d.find(self.bufferSignature2)
            print '\n'
        except Exception, e:
            print str(e)
        # decrypt contents if encryption is turned on
        if self.encryptionEnabled:
            # print message if we have a good idea what it is
            if bufferIndex != -1:
                try:
                    fileContents = self.decrypt_message(d[:bufferIndex], self.password)
                    if self.verbose:
                        print '[+] Message: \n' + fileContents
                except Exception, e:
                    print '\033[0;31m[!] Failed to decrypt contents\033[0m'
                    logger.error('Failed to decrypt contents during file assembly: {}'.format(str(e)))
                    print str(e)
            else:
                print '\n\033[0;31m[?] failed to find buffer.. printing possible message\033[0m'
                logger.warn('Failed to find message buffers...')
                print '[+] \n' + d[:200]

            # print file type if we have a good idea
            if bufferIndex2 != -1:
                if self.verbose:
                    print '[+] file type: ' + d[bufferIndex+48:bufferIndex2]
                hiddenFileType = '.' + d[bufferIndex+49:bufferIndex2-1]
            else:
                print '\033[0;31m[?] unknown file type\033[0m'
                logger.warn('Unknown file type in extracted message')
            if (bufferIndex != -1) and (bufferIndex2 != -1):
                try:
                    newSave = open(location + 'hiddenFile' + hiddenFileType,'w')
                    self.ExtractedFileName = 'hiddenFile'+hiddenFileType
                    newSave.write(fileContents)
                    newSave.close()
                    print '[+] See hidden file contents: {}{}'.format('hiddenFile',hiddenFileType)
                    logger.info('Successfully extracted message: {}{}'.format('hiddenFile',hiddenFileType))
                except Exception, e:
                    print '\033[0;31m[!] Failed to write extracted file to new location\033[0m'
                    logger.error('Failed to write extracted file: {}'.format(str(e)))
                    print str(e)
                    return '.'
                finally:
                    newSave.close()
                    return 'hiddenFile'+hiddenFileType
        else:
            # print message if we have a good idea what it is
            if bufferIndex != -1:
                if self.verbose:
                    print '\n[+] Message: \n' + d[:bufferIndex]
                fileContents = d[:bufferIndex]
            else:
                print '\n\033[0;31m[?] failed to find buffer.. printing possible message\033[0m'
                logger.warn('Failed to find message buffers...')
                print '[+] \n' + d[:200]

            # print file type if we have a good idea
            logger.warn('Cannot find exact buffer, making our best guess')
            if bufferIndex2 != -1:
                if self.verbose:
                    print '[+] file type: ' + d[bufferIndex+48:bufferIndex2]
                hiddenFileType = '.' + d[bufferIndex+49:bufferIndex2-1]
            else:
                print '\033[0;31m[?] unknown file type\033[0m'
                logger.warn('Unknown file type in extracted message')
                print d[bufferIndex+48:(bufferIndex+48)+10]

            if (bufferIndex != -1) and (bufferIndex2 != -1):
                try:
                    newSave = open('hiddenFile'+hiddenFileType,'w')
                    self.ExtractedFileName = 'hiddenFile'+hiddenFileType
                    newSave.write(location + fileContents)
                    newSave.close()
                    print '[+] See hidden file contents: %s%s' % ('hiddenFile',hiddenFileType)
                    logger.info('Successfully extracted message: {}{}'.format('hiddenFile',hiddenFileType))
                except Exception, e:
                    print '\033[0;31m[!] Failed to write extracted file to new location\033[0m'
                    logger.error('Failed to write extracted file: {}'.format(str(e)))
                    print str(e)
                    return '.'
                finally:
                    newSave.close()
                    return 'hiddenFile'+hiddenFileType


    # When seeded with all available file extensions provides the values for return_correct_object
    def _seed_supported_types(self, *args):
        self.supportedFileTypes = {}
        for arg in args:
           self.supportedFileTypes.update(arg)

    # returns the correct object to use for a provided file type
    def return_correct_object(self,fileToHide=None,returnLocation=None):
        if self.fileType in self.supportedFileTypes["StegIMG"]:
            return StegIMG.StegIMG(fileToHide,self.HostFile,self.password,self.encryptionMode,self.verbose,returnLocation)
        elif self.fileType in self.supportedFileTypes["StegAIF"]:
            return StegAIF.StegAIF(fileToHide,self.HostFile,self.password,self.encryptionMode,self.verbose,returnLocation)
        elif self.fileType in self.supportedFileTypes["StegWav"]:
            return StegWav.StegWav(fileToHide,self.HostFile,self.password,self.encryptionMode,self.verbose,returnLocation)
        else:
            return None
