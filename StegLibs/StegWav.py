#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software

import StegCommon
import wave
import os
import array
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('stegosys.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class StegWav:
    ''' Class for hiding and extracting binary data in WAV files
    '''


    def __init__(self, filePath, audioPath, password=None, encryptionMode="ARC4",verbose=False, returnLocation=None):
        # name some constants
        self.preMD5 = ''
        self.postMD5 = ''
        self.fileToHide = filePath
        self.audioToUse = audioPath
        self.password = password
        self.verbose = verbose
        self.returnLocation = returnLocation
        logger.info('Host file: ' + self.audioToUse)

        # get file type from file path
        if self.fileToHide != None:
            logger.info('File to hide: ' + self.fileToHide)
            self.fileType = filePath.split('.')[-1]

        self.common = StegCommon.StegCommon(self.fileToHide, self.password, encryptionMode, self.verbose)

    def _open_audio(self, audioPath):
        try:
            self.audioType = (audioPath.split('.')[-1]).lower()
            if (self.audioType == 'wav') | (self.audioType == 'wave'):
                self.au = wave.open(audioPath,'rb')
                self.channels = self.au.getnchannels()
                self.width = self.au.getsampwidth()
                self.framerate = self.au.getframerate()
                self.framecount = self.au.getnframes()
                self.length = self.framecount/self.framerate
                self.compressionType = self.au.getcomptype()
                self.compressionName = self.au.getcompname()
                self.preMD5 = self.common.return_MD5(audioPath)
                self.maxSize = (self.framecount * self.channels)
                logger.info('Successfully opened AIF')
                logger.info('Channels: {}, Width: {}, FrameRate: {}, FrameCount: {}, Compression: {} '.format(self.channels, self.width, self.framerate, self.framecount, self.compressionName))
                logger.debug('Pre-Operation MD5: {}'.format(str(self.preMD5)))
        except Exception as e:
            print '[!] Error opening audio file'
            logger.error('Error opening AIF: {}'.format(str(e)))
            print str(e)


    def display_wav_info(self,wave):
        print "Channels: " + str(wave.getnchannels())
        print "Sample Width: " + str(wave.getsampwidth())
        print "Frame Rate: " + str(wave.getframerate())
        print "Frame Count: " + str(wave.getnframes())
        print "Length (seconds): " + str(wave.getnframes()/wave.getframerate())
        print "Compression Type: " + str(wave.getcomptype())
        print "Compression Name: " + str(wave.getcompname())
        print "Hash: " + self.preMD5



    def generate_matching_frames(self,width,channels,ba):
            '''
            Mono:
                Channel1 #0,1,2,3,4,5,6 > #1,3,5,7,9 > #2,5,8,11 > #3,7,11,15
            Stereo:
                Channel1 #0,2,4,6,8,10 > #2,6,10,14 >  #4,10,16,22 > #6,14,22
                Channel2 #1,3,5,7,9,11 > #3,7,11,15 > #5,11,17,23 > #7,15,23
            3x:
                Channel1 #0,3,6,9 > #3,9,15 > #6,15,24 > #9,21,33
                Channel2 #1,4,7,10 > #4,10,16 > #7,16,25 > #10,22,34
                Channel3 #2,5,8,11 > #5,11,17 > #8,17,25 > #11,23,35

            Right now I'm only really supporting up to 3 channels.. and I still think that's probably wrong
            and woefully inefficient :(
            '''
            # seeds the starting position for the first channel
            startingPosition = [[0,1,2,3],[0,2,4,6],[0,3,6,9],[0,4,8,12]]
            # create empty lists for channels
            self.ch1 = []
            self.ch2 = []
            self.ch3 = []
            #self.ch4 = []

            if width > 0:
                for i in range((startingPosition[channels-1][width-1]),len(ba),(width*channels)):
                    self.ch1.append(i)
                for i in range((startingPosition[channels-1][width-1]+1),len(ba),(width*channels)):
                    self.ch2.append(i)
                for i in range((startingPosition[channels-1][width-1]+2),len(ba),(width*channels)):
                    self.ch3.append(i)
                #for i in range((startingPosition[channels-1][width-1]+3),len(ba),(width*channels)):
                    #self.ch4.append(i)
            print self.ch1[0:5]
            print self.ch2[0:5]

    def _replace_bits(self, msgToHide, type, location):
        # how many bits can be hidden in the file
        # This is caluclated by how the framerate/bytes per sample * the length of the file (seconds)
        maxSize = (self.framecount * self.channels)

        if maxSize <= 2*len(msgToHide):
            print '\033[0;31m[!] Attempting to hide a message that is too large for the host\033[0m'
            print '[!] Terminating'
            logger.error('Attempting to hide a message that is too large for the host')
            exit(0)

        # generate bitstream
        bitstream = iter(self.common.text_to_binary(msgToHide, maxSize * 2))

        # extract all bytes raw
        bs = self.au.readframes(self.framecount)

        # convert to a list of all bytes in ordformat
        ba = map(ord, bs)

        # creates the correct frame matching depending on the file format
        self.generate_matching_frames(self.width,self.channels,ba)


        # iterate through all samples in the file and replace every lsb that matches the matches the mapping
        # in the generate_matching_frames definition.
        logger.info('Starting to hide message')
        if self.channels == 1:
            for i in self.ch1:
                # if the byte we're looking at is the not the last byte
                if i < len(ba):
                    try:
                        ba[i] = self.common.set_bit(ba[i], bitstream.next())
                    except StopIteration:
                        print '\033[0;31m[!] Bitstream expired'
                        logger.error('Bitstream Expired! Error in Maxsize calculation or bitstream generation!')
                        exit(0)
        elif self.channels == 2:
            # for more than 1 channel we need to only touch the bytes associated with that channel, hence the step size
            self.ch1.extend(self.ch2)
            ch1ch2 = sorted(self.ch1)
            for i in ch1ch2:
                try:
                    ba[i] = self.common.set_bit(ba[i], bitstream.next())
                except StopIteration:
                    print '\033[0;31m[!] Bitstream expired'
                    logger.error('Bitstream Expired! Error in Maxsize calculation or bitstream generation!')
                    exit(0)
        elif self.channels == 3:
            self.ch1.extend(self.ch2)
            self.ch1.extend(self.ch3)
            ch1ch2ch3 = sorted(self.ch1)
            for i in ch1ch2ch3:
                try:
                    ba[i] = self.common.set_bit(ba[i], bitstream.next())
                except StopIteration:
                    print '\033[0;31m[!] Bitstream expired'
                    logger.error('Bitstream Expired! Error in Maxsize calculation or bitstream generation!')
                    exit(0)

        outdata = array.array('B',ba).tostring()

        try:
            if os.path.isdir(location) and os.access(location, os.W_OK):
                outfile = location + 'new_audio.{}'.format(type)
            else:
                outfile = 'new_audio.{}'.format(type)
            out = wave.open(outfile,'w')
            out.setparams(self.au.getparams())
            out.writeframesraw(outdata)
            if self.common.return_MD5(outfile) != self.preMD5:
                print '\n[+] {} created'.format(outfile)
                self.NewFileWithHidden = outfile
                print '[+] Message successfully hidden'
                logger.info('Message hidden successfully')
                logger.info('{} created'.format(outfile))
            else:
                print '\033[0;31m[!] Hashing Error'
                logger.warn('Issue comparing pre and post hashes {} : {}'.format(str(self.common.return_MD5(outfile)),self.preMD5))
        except Exception, e:
            print '\033[0;31m[!] Failed to write new file\033[0m'
            logger.error('Failed to write new file')
            print str(e)
        finally:
            out.close()


    def _extract_message(self):
        hidden = ''
        try:
            # extract all bytes raw
            bs = self.au.readframes(self.framecount)

            # convert to a list of all bytes in ordformat
            ba = map(ord, bs)

            # we need to know which bytes we care about
            self.generate_matching_frames(self.width,self.channels,ba)

            # Collect the bits we originally wrote
            if self.channels == 1:
                for i in self.ch1:
                    # if the byte we're looking at is the not the last byte
                    if i < len(ba):
                        try:
                            hidden += bin(ba[i])[-1]
                        except Exception, e:
                            logger.error('Error extracting bytes from mono: {}'.format(str(e)))
                            print str(e)
            elif self.channels == 2:
                # for more than 1 channel we need to only collect the bytes associated with that channel, hence the step size
                self.ch1.extend(self.ch2)
                ch1ch2 = sorted(self.ch1)
                for i in ch1ch2:
                    try:
                        hidden += bin(ba[i])[-1]
                    except Exception, e:
                        logger.error('Error extracting bytes from stereo: {}'.format(str(e)))
                        print str(e)
                print hidden
            elif self.channels == 3:
                self.ch1.extend(self.ch2)
                self.ch1.extend(self.ch3)
                ch1ch2ch3 = sorted(self.ch1)
                for i in ch1ch2ch3:
                    try:
                        hidden += bin(ba[i])[-1]
                    except Exception, e:
                        logger.error('Error extracting bytes from 3 channel wav: {}'.format(str(e)))
                        print str(e)
            try:
                return_file = self.common.reconstitute_from_binary(hidden, self.returnLocation)
                logger.info('Successfully reconstituted message from binary')
                return return_file
            except Exception, e:
                print '\033[0;31m[!] INNER failed to extract message\033[0m'
                logger.error('Failed to extract message: {}'.format(str(e)))
                print str(e)
                return '.'
        except Exception, e:
            print '\033[0;31m[!] failed to extract message\033[0m'
            logger.error('Failed to extract message: {}'.format(str(e)))
            print str(e)
            return '.'






class StegWAVHelper:
    ''' Supplies helper functions for StegWav'''

    def __init__(self):
        self.SupportedFileTypes = ['WAV', 'WAVE']
        self.verbose = True

    # Returns a list of supported file formats
    def get_supported_file_types(self):
        return self.SupportedFileTypes

    def get_supported_file_types_dict(self):
        return {"StegWav" : self.SupportedFileTypes}

    def is_file_supported(self, fileExtension):
        if fileExtension in self.SupportedFileTypes:
            if self.verbose:
                print '[+] File type supported: %s' % fileExtension
            return True
        else:
            if self.verbose:
                print '[!] File type not supported'
                print self.SupportedFileTypes
            return False

        # returns the total space available to hide data within the desired image
    def return_max_size(self, filepath):
        au = wave.open(filepath)
        self.channels = au.getnchannels()
        self.width = au.getsampwidth()
        self.framerate = au.getframerate()
        self.framecount = au.getnframes()
        self.length = self.framecount/self.framerate
        return self.framerate/self.width * self.length