#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software
VERSION = '0.1'

import argparse
import logging
import getpass
import StegIMG
import StegWav
import StegAIF
import StegCommon


# Start Logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('stegosys.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.info('Starting Stegosys v' + VERSION)


# Logo
print '\033[0;32m\n\n'
print '  ____  _                                  '
print ' / ___|| |_ ___  __ _  ___  ___ _   _ ___  '
print ' \___ \| __/ _ \/ _` |/ _ \/ __| | | / __| '
print '  ___) | ||  __| (_| | (_) \__ | |_| \__ \ '
print ' |____/ \__\___|\__, |\___/|___/\__, |___/ '
print '                |___/           |___/  '
print ' Author: Andrew Scott 11/28/2015'
print ' v {}\033[0m\n\n'.format(VERSION)

# Option parser
argparser = argparse.ArgumentParser(description="Stegosys is a tool for hiding and extracting data in media.")

group = argparser.add_mutually_exclusive_group(required=True)
group.add_argument('--h', dest='mode1', action='store_true', default=False, help='hide mode')
group.add_argument('--e', dest='mode2', action='store_true', default=False, help='extract mode')
group.add_argument('--i', dest='mode3', action='store_true', default=False, help='info mode')

argparser.add_argument('-I', dest='inputFile', type=str, help='path to host file')
argparser.add_argument('-x', dest='encryptMode', type=str, help='use encryption -> ARC4, AES')
argparser.add_argument('-H',dest='hideFile', type=str ,help='file to hide')
argparser.add_argument('-a', dest='isAudioMode', action='store_true', default=False, help='audio mode')
argparser.add_argument('-p', dest='isImageMode', action='store_true', default=False, help='image mode')
argparser.add_argument('-v', dest='isVideoMode', action='store_true', default=False, help='video mode')
argparser.add_argument('-V', dest='verbose', action='store_true', default=False, help='verbose output')


args = argparser.parse_args()
inputFilePath = args.inputFile
msgToHide = args.hideFile
mode1 = args.mode1
mode2 = args.mode2
mode3 = args.mode3
isAudioMode = args.isAudioMode
isImageMode = args.isImageMode
isVideoMode = args.isVideoMode
verbose = args.verbose
toEncrypt = None
if args.encryptMode != None:
    toEncrypt = args.encryptMode


if (mode1 == False) and (mode2 == False) and (mode3 == False):
    print '\033[0;31m[!] no mode selected\033[0m'
    logger.error('No mode selected')
    raise argparse.ArgumentTypeError('No mode selected')
else:
    if mode1:
        programMode = 'Hide'
        print '[+] Hide mode selected'
        logger.info('Hide mode selected')
    elif mode2:
        programMode = 'Extract'
        print '[+] Extract mode selected'
        logger.info('Extract mode selected')
    elif mode3:
        programMode = 'Info'
        print '[+] Info mode selected'
        logger.info('Info mode selected')
    else:
        exit(0)

if (inputFilePath == None):
    logger.error('No input file designated')
    raise argparse.ArgumentTypeError('No input file designated')

if (programMode == 'Hide') and (msgToHide == None):
    logger.error('No file to hide designated')
    raise argparse.ArgumentTypeError('No file to hide designated')


logger.info('Host file: ' + inputFilePath)
if msgToHide != None:
    logger.info('File to hide: ' + msgToHide)


def guessMode(inputFile):
    ## TODO try to determine how to proceed based on inut file type
    pass

# main
def main():
    # create stegcommon object for any helper functions
    common = StegCommon.StegCommon(inputFilePath)
    inputFileExtension = common.get_file_extension_host()
    global password
    if toEncrypt != None:
        print '[+] Please enter encryption key: '
        password = getpass.getpass()
        logger.info('Encryption enabled with type - ' + toEncrypt)
    else:
        password = None
    if programMode == 'Hide':
        if isImageMode:
            logger.info('Image mode selected')
            SH = StegIMG.StegIMGHelper()
            if SH.is_file_supported(inputFileExtension):
                S = StegIMG.StegIMG(msgToHide,inputFilePath,password,toEncrypt,verbose)
                S.open_image(inputFilePath)
                if inputFileExtension == 'PNG':
                    print '[+] png detected'
                    logger.info('Starting file hiding in ' + inputFileExtension)
                    S._use_correct_function_hide(S.fg, msgToHide, 'png')
                elif (inputFileExtension == 'TIF') | (inputFileExtension.lower() == 'TIFF'):
                    print '[+] TIFF detected'
                    logger.info('Starting file hiding in ' + inputFileExtension)
                    S._use_correct_function_hide(S.fg, msgToHide, 'TIFF')
                elif (inputFileExtension == 'BMP'):
                    print '[+] BMP detected'
                    logger.info('Starting file hiding in ' + inputFileExtension)
                    S._use_correct_function_hide(S.fg, msgToHide, 'bmp')
                elif (inputFileExtension == 'ICO'):
                    print '[+] ICO detected'
                    logger.info('Starting file hiding in ' + inputFileExtension)
                    S._use_correct_function_hide(S.fg, msgToHide, 'ICO')
            else:
                print '\033[0;31m[!] Unknown or Unsupported File Type\033[0m'
                logger.error('Unknown or Unsupported file type: ' + inputFileExtension)
                exit(0)
        if isAudioMode:
            logger.info('Audio mode selected')
            AH_WAV = StegWav.StegWAVHelper()
            AH_AIF = StegAIF.StegAIFHelper()
            if AH_WAV.is_file_supported(inputFileExtension):
                print '[+] WAV detected'
                logger.info('Starting file hiding in ' + inputFileExtension)
                A = StegWav.StegWav(msgToHide,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A._replace_bits(msgToHide, 'wav')
            elif AH_AIF.is_file_supported(inputFileExtension):
                print '[+] AIFF detected'
                logger.info('Starting file hiding in ' + inputFileExtension)
                A = StegAIF.StegAIF(msgToHide,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A._replace_bits(msgToHide, 'aif')
            else:
                print '\033[0;31m[!] Unknown or Unsupported File Type\033[0m'
                logger.error('Unknown or Unsupported file type: ' + inputFileExtension)
                exit(0)
        if isVideoMode:
            print "[!] video mode not yet supported"
            exit(0)
        else:
            guessMode(inputFilePath)



    elif programMode == 'Extract':
        if isImageMode:
            logger.info('Image mode selected')
            S = StegIMG.StegIMG(None,inputFilePath,password,toEncrypt,verbose)
            S.open_image(inputFilePath)
            if (S.imageType.lower() == 'jpeg') | (S.imageType.lower() == 'jpg'):
                print '[+] jpeg detected'
                print '\033[0;31m[!] Sorry, Stegosys v 0.1 does not support jpeg/jpg\033[0m'
                exit(0)
            elif S.imageType.lower() == 'png':
                print '[+] PNG detected'
                logger.info('Starting file extraction from ' + inputFileExtension)
                S._use_correct_function_extract(S.fg)
            elif (S.imageType.lower() == 'tif') | (S.imageType.lower() == 'tiff'):
                print '[+] TIFF detected'
                logger.info('Starting file extraction from ' + inputFileExtension)
                S._use_correct_function_extract(S.fg)
            elif S.imageType.lower() == 'bmp':
                print '[+] BMP detected'
                logger.info('Starting file extraction from ' + inputFileExtension)
                S._use_correct_function_extract(S.fg)
            elif S.imageType.lower() == 'ico':
                print '[+] ICO detected'
                logger.info('Starting file extraction from ' + inputFileExtension)
                S._use_correct_function_extract(S.fg)
            else:
                print '\033[0;31m[!] Unknown File Type\033[0m'
                logger.error('Unknown or Unsupported file type: ' + inputFileExtension)
                exit(0)
        if isAudioMode:
            logger.info('Audio mode selected')
            AH_WAV = StegWav.StegWAVHelper()
            AH_AIF = StegAIF.StegAIFHelper()
            if AH_WAV.is_file_supported(inputFileExtension):
                logger.info('Starting file extraction from ' + inputFileExtension)
                A = StegWav.StegWav(None,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A._extract_message()
            elif AH_AIF.is_file_supported(inputFileExtension):
                logger.info('Starting file extraction from ' + inputFileExtension)
                A = StegAIF.StegAIF(None,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A._extract_message()
        if isVideoMode:
            print "[!] video mode not yet supported"
        else:
            guessMode(inputFilePath)



    elif programMode == 'Info':
        if isImageMode:
            logger.info('Image mode selected')
            S = StegIMG.StegIMGHelper()
            if S.is_file_supported(inputFileExtension):
                availableSpace = S.return_max_size(inputFilePath)
                if availableSpace > 0:
                    print '[+] Total space available to hide: %s' % common.humanize_file_size(availableSpace)
                else:
                    print '[!] Could not determine space available.'
                    logger.error('Could not determine space available to hide')
        if isAudioMode:
            logger.info('Audio mode selected')
            AH_WAV = StegWav.StegWAVHelper()
            AH_AIF = StegAIF.StegAIFHelper()
            if AH_WAV.is_file_supported(inputFileExtension):
                A = StegWav.StegWav(None,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A.display_wav_info(A.au)
                availableSpace = AH_WAV.return_max_size(inputFilePath)
                if availableSpace > 0:
                    print '[+] Total space available to hide: %s' % common.humanize_file_size(availableSpace)
                else:
                    print '[!] Could not determine space available.'
                    logger.error('Could not determine space available to hide')
            elif AH_AIF.is_file_supported(inputFileExtension):
                A = StegAIF.StegAIF(None,inputFilePath,password,toEncrypt,verbose)
                A._open_audio(inputFilePath)
                A.display_aif_info(A.au)
                availableSpace = AH_AIF.return_max_size(inputFilePath)
                if availableSpace > 0:
                    print '[+] Total space available to hide: %s' % common.humanize_file_size(availableSpace)
                else:
                    print '[!] Could not determine space available.'
                    logger.error('Could not determine space available to hide')
        if isVideoMode:
            pass
        else:
            guessMode(inputFilePath)
    else:
        print '[!] No mode selected!'
        exit(0)


if __name__ == '__main__':
    main()

