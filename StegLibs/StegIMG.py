#!/usr/bin/python
# -*- coding: utf-8 -*-

## Author: Andrew Scott
## Repository: https://bitbucket.org/beatsbears/stegosys
## Copyright Andrew Scott 2016
## Drowned Coast Software

from PIL import Image as img
import os
import StegCommon
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.FileHandler('stegosys.log')
handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)



class StegIMG:
    ''' Class for hiding binary data with select lossless image formats
    ------ Currently supported image formats: PNG, TIFF, BMP ------------
    '''

    def __init__(self, filePath, imagePath, password=None, encryptionMode="ARC4",verbose=False, returnLocation=None):
        # name some constants
        self.preMD5 = ''
        self.postMD5 = ''
        self.fileToHide = filePath
        self.imageToUse = imagePath
        self.password = password
        self.verbose = verbose
        self.returnLocation = returnLocation
        logger.info('Host file: ' + self.imageToUse)

        # get image type from image path
        if self.fileToHide != None:
            logger.info('File to hide: ' + self.fileToHide)
            self.fileType = filePath.split('.')[-1]

        self.common = StegCommon.StegCommon(self.fileToHide, self.password, encryptionMode, self.verbose)

    def open_image(self, imagePath):
        try:
            self.fg = img.open(imagePath)
            self.imageType = imagePath.split('.')[-1]
            self.preMD5 = self.common.return_MD5(imagePath)
            # total number of pixels that can be manipulated
            self.maxSize = self.fg.size[1] * self.fg.size[0]
            # returns the image mode, hopefully this is L, RGB, or RGBA
            self.imageMode = ''.join(self.fg.getbands())
            logger.info('Successfully opened {}'.format(self.imageType))
            logger.debug('Pre-Operation MD5: {}'.format(str(self.preMD5)))
        except Exception as e:
            print '[!] Error opening image'
            logger.error('Error opening Image: {}'.format(str(e)))
            print str(e)

    #PIL can be particular about image format extensions
    def assign_output_file_type(self, imageType):
        imageType = imageType.lower()
        if (imageType == 'jpg') | (imageType == 'jpeg'):
            return 'jpeg'
        elif (imageType == 'tif') | (imageType == 'tiff'):
            return 'TIFF'
        elif (imageType == 'png'):
            return 'png'
        elif (imageType == 'bmp'):
            return 'bmp'

    # depending on IMG format will use the different Hiding methods
    def _use_correct_function_hide(self, fg, msgToHide, type, location):
        if self.imageMode == 'L':
            self.L_replace_bits(fg, msgToHide, type, location)
        elif (self.imageMode == 'RGB') or (self.imageMode == 'BGR'):
            self.RGB_replace_bits(fg, msgToHide, type, location)
        elif self.imageMode == 'RGBA':
            self.RGBA_replace_bits(fg, msgToHide, type, location)
        elif self.imageMode == '1':
            print "[!] Cannot hide content using an image with a mode of '1'"
            logger.error("Cannot hide content using an image with a mode of '1'")
            exit(0)
        else:
            logger.error("Error determining image mode")
            exit(0)

    # depending on IMG format will use the different Extraction methods
    def _use_correct_function_extract(self, fg, location):
        if self.imageMode == 'L':
            return self.L_extract_message(fg,location)
        elif (self.imageMode == 'RGB') or (self.imageMode == 'BGR'):
            return self.RGB_extract_message(fg,location)
        elif self.imageMode == 'RGBA':
            return self.RGBA_extract_message(fg, location)
        elif self.imageMode == '1':
            print "[!] Cannot extract content using an image with a mode of '1'"
            logger.error("Cannot extract content using an image with a mode of '1'")
            return '.'
        else:
            logger.error("Error determining image mode")
            exit(0)

    # replace lest-significant bit for RGB images
    def RGB_replace_bits(self, fg, msgToHide, type, location):
        # get total available bits to hide data
        maxSize = fg.size[1] * fg.size[0]

        # 3 bytes per pixel should be greater than 2* the binary message length
        if maxSize*3 <= 2*len(msgToHide):
            print '\033[0;31m[!] Attempting to hide a message that is too large for the host\033[0m'
            logger.error('Attempting to hide a message that is too large for the host')
            print '[!] Terminating'
            exit(0)

        # generate bitstream 
        bitstream = iter(self.common.text_to_binary(msgToHide, maxSize * 3))

        showme = ''
        # create a new empty image the same size and mode as the original
        newIm = img.new("RGB", (fg.size[0], fg.size[1]), "white") 
        print '[+] starting to hide message'
        logger.info('Starting to hide message')
        try:
            count = 0
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    # get the value for each byte of each pixel in the original image
                    fgr,fgg,fgb = fg.getpixel((col,row))

                    # get the new lsb value from the bitstream
                    redbit = bitstream.next()
                    # modify the original byte with our new lsb
                    fgr = self.common.set_bit(fgr, redbit)

                    greenbit = bitstream.next()
                    fgg = self.common.set_bit(fgg, greenbit)
                    showme += bin(fgg)

                    bluebit = bitstream.next()
                    fgb = self.common.set_bit(fgb, bluebit)
                    showme += bin(fgb)
                    # add pixel with modified values to new image
                    newIm.putpixel((col,row),(fgr,fgg,fgb))
                count += 1
                if count % 20 == 0:
                    print '.',
            #if our passed in location exists, try saving there
            if os.path.isdir(location) and os.access(location, os.W_OK):
                newIm.save(str(location + 'newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            else:
                newIm.save(str('newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            if self.common.return_MD5(str('newImg.'+type)) != self.preMD5:
                print '\n[+] {} created'.format('newImg.'+type)
                self.NewFileWithHidden = str('newImg.'+type)
                print '[+] Message successfully hidden'
                logger.info('Message successfully hidden')
                logger.info('{} created'.format('newImg.'+type))
        except Exception, e:
            print '\n\033[0;31m[!] Message write failed!\033[0m'
            logger.error('Failed to write new file: {}'.format(str(e)))
            print str(e)


    # extract hidden message from RGB image
    def RGB_extract_message(self, fg, location):
        hidden = ''
        try:
            # set up a counter so we can animate for big files
            count = 0
            # iterate through each pixel and pull the lsb from each color per pixel
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    fgr,fgg,fgb = fg.getpixel((col,row))
                    
                    hidden += bin(fgr)[-1]
                    hidden += bin(fgg)[-1]
                    hidden += bin(fgb)[-1]

                count += 1
                if count % 20 == 0:
                    print '.',  
            try:
                returned_file = self.common.reconstitute_from_binary(hidden, location)
                return returned_file
            except Exception, e:
                print '\033[0;31m[!] INNER failed to extract message\033[0m'
                logger.error('Inner failed to extract message: {}'.format(str(e)))
                print str(e)
        except Exception, e:
            print '\033[0;31m[!] OUTER failed to extract message\033[0m'
            logger.error('Outer failed to extract message: {}'.format(str(e)))
            print str(e)

    #LSB steg for Black and white images
    def L_replace_bits(self, fg, msgToHide, type, location):
        # get total available bits to hide data
        maxSize = fg.size[1] * fg.size[0]

        if maxSize <= 2*len(msgToHide):
            print '\033[0;31m[!] Attempting to hide a message that is too large for the host\033[0m'
            print '[!] Terminating'
            exit(0)

        # generate bitstream 
        bitstream = iter(self.common.text_to_binary(msgToHide, maxSize * 3))

        showme = ''
        newIm = img.new("L", (fg.size[0], fg.size[1]), "white") 
        print '[+] starting to hide message'
        try:
            count = 0
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    fgL = fg.getpixel((col,row))

                    nextbit = bitstream.next()
                    fgL = self.common.set_bit(fgL, nextbit)
                    showme += bin(fgL)

                    # add pixel to new image
                    newIm.putpixel((col,row),(fgL))
                count += 1
                if count % 20 == 0:
                    print '.',
            #if our passed in location exists, try saving there
            if os.path.isdir(location) and os.access(location, os.W_OK):
                newIm.save(str(location + 'newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            else:
                newIm.save(str('newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            if self.common.return_MD5(str('newImg.'+type)) != self.preMD5:
                print '\n[+] %s created' % str('newImg.'+ str(type))
                self.NewFileWithHidden = str('newImg.'+type)
                print '[+] Message successfully hidden'
                logger.info('Message successfully hidden')
                logger.info('{} created'.format('newImg.'+type))
        except Exception, e:
            print '\n\033[0;31m[!] Message write failed!\033[0m'
            logger.error('Failed to write new file: {}'.format(str(e)))
            print str(e)

    # extract hidden message from L image
    def L_extract_message(self, fg, location):
        hidden = ''
        try:
            # set up a counter so we can animate for big files
            count = 0
            # iterate through each pixel and pull the lsb from each color per pixel
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    fgL = fg.getpixel((col,row))
                    
                    hidden += bin(fgL)[-1]

                count += 1
                if count % 20 == 0:
                    print '.',  
            try:
                returned_file = self.common.reconstitute_from_binary(hidden, location)
                return returned_file
            except Exception, e:
                print '\033[0;31m[!] INNER failed to extract message\033[0m'
                logger.error('Inner failed to extract message: {}'.format(str(e)))
                print str(e)
        except Exception, e:
            print '\033[0;31m[!] OUTER failed to extract message\033[0m'
            logger.error('Outer failed to extract message: {}'.format(str(e)))
            print str(e)

    # replace lest-significant bit for RGBA images
    def RGBA_replace_bits(self, fg, msgToHide, type, location):
        # get total available bits to hide data
        maxSize = fg.size[1] * fg.size[0]

        if maxSize*3 <= 2*len(msgToHide):
            print '\033[0;31m[!] Attempting to hide a message that is too large for the host\033[0m'
            print '[!] Terminating'
            exit(0)

        # generate bitstream 
        bitstream = iter(self.common.text_to_binary(msgToHide, maxSize * 3))

        showme = ''
        newIm = img.new("RGB", (fg.size[0], fg.size[1]), "white") 
        print '[+] starting to hide message'
        try:
            count = 0
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    fgr,fgg,fgb,fga = fg.getpixel((col,row))

                    redbit = bitstream.next()
                    fgr = self.common.set_bit(fgr, redbit)

                    greenbit = bitstream.next()
                    fgg = self.common.set_bit(fgg, greenbit)
                    showme += bin(fgg)

                    bluebit = bitstream.next()
                    fgb = self.common.set_bit(fgb, bluebit)
                    showme += bin(fgb)
                    # add pixel to new image
                    newIm.putpixel((col,row),(fgr,fgg,fgb))
                count += 1
                if count % 20 == 0:
                    print '.'
            #if our passed in location exists, try saving there
            if os.path.isdir(location) and os.access(location, os.W_OK):
                newIm.save(str(location + 'newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            else:
                newIm.save(str('newImg.' + self.assign_output_file_type(type)), self.assign_output_file_type(type))
            if self.common.return_MD5(str('newImg.'+type)) != self.preMD5:
                print '\n[+] %s created' % str('newImg.'+type)
                self.NewFileWithHidden = str('newImg.'+type)
                print '[+] Message successfully hidden'
                logger.info('Message successfully hidden')
                logger.info('{} created'.format('newImg.'+type))
        except Exception, e:
            print '\n\033[0;31m[!] Message write failed!\033[0m'
            logger.error('Failed to write new file: {}'.format(str(e)))
            print str(e)


    # extract hidden message from RGBA image
    def RGBA_extract_message(self, fg, location):
        hidden = ''
        try:
            # set up a counter so we can animate for big files
            count = 0
            # iterate through each pixel and pull the lsb from each color per pixel
            for row in range(fg.size[1]):
                for col in range(fg.size[0]):
                    fgr,fgg,fgb = fg.getpixel((col,row))
                    
                    hidden += bin(fgr)[-1]
                    hidden += bin(fgg)[-1]
                    hidden += bin(fgb)[-1]

                count += 1
                if count % 20 == 0:
                    print '.',  
            try:
                returned_file = self.common.reconstitute_from_binary(hidden, location)
                return returned_file
            except Exception, e:
                print '\033[0;31m[!] INNER failed to extract message\033[0m'
                logger.error('Inner failed to extract message: {}'.format(str(e)))
                print str(e)
        except Exception, e:
            print '\033[0;31m[!] OUTER failed to extract message\033[0m'
            logger.error('Outer failed to extract message: {}'.format(str(e)))
            print str(e)




class StegIMGHelper:
    ''' Supplies helper functions for StegIMG'''

    def __init__(self):
        self.SupportedFileTypes = ['PNG','TIFF','TIF','BMP','ICO']
        self.verbose = True

    # Returns a list of currently supported image formats
    def get_supported_file_types(self):
        return self.SupportedFileTypes

    def get_supported_file_types_dict(self):
        return {"StegIMG" : self.SupportedFileTypes}

    def is_file_supported(self, fileExtension):
        if fileExtension in self.SupportedFileTypes:
            if self.verbose:
                print '[+] File type supported: %s' % fileExtension
            return True
        else:
            if self.verbose:
                print '[!] File type not supported'
                print self.SupportedFileTypes
            return False

    # returns the total space available to hide data within the desired image
    def return_max_size(self, filepath):
        fg = img.open(filepath)
        imageMode = ''.join(fg.getbands())
        maxSize = fg.size[1] * fg.size[0]
        if imageMode == 'L':
            return (maxSize/2)/8
        elif imageMode == 'RGB' or imageMode == 'BGR':
            return ((maxSize*3)/2)/8
        elif imageMode == 'RGBA':
            return ((maxSize*3)/2)/8
        else:
            return 0
